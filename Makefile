REG_ADDRESS=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

drupal-composer-build:
	docker-compose -f drupal-composer.yml build

drupal-composer-up:
	docker-compose -f drupal-composer.yml up

drupal-composer-tag:
	docker tag drupal:latest ${REG_ADDRESS}/${USERNAME}/docker-drupal-composer-images:base-${VERSION} && \
	docker tag ${REG_ADDRESS}/${USERNAME}/docker-drupal-composer-images:base-${VERSION} ${REG_ADDRESS}/${USERNAME}/docker-drupal-composer-images:base-latest

drupal-composer-push:
	docker push ${REG_ADDRESS}/${USERNAME}/docker-drupal-composer-images:base-${VERSION} && \
	docker push ${REG_ADDRESS}/${USERNAME}/docker-drupal-composer-images:base-latest
